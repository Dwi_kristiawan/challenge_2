import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.BufferedWriter;

public interface ReadAndWrite {
    static String inputan() {
        Scanner scan=new Scanner(System.in);
        return scan.next();
    }

    static void homeView() {
        System.out.println("_______________________________________________________________");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("_______________________________________________________________");
        System.out.println("Letakkan File csv dengan nama data_sekolah di direktori berikut");
        System.out.println("Directori:src");
        System.out.println();
        System.out.println("Pilih Menu");
        System.out.println("1. Generate txt untuk menampilkan nilai Modus");
        System.out.println("2. Generate txt untuk menampilkan nilai Rata-rata dan Median ");
        System.out.println("3. Generate keduanya");
        System.out.println("0. exit");

    }
    static void menuView() {
        System.out.println("_______________________________________________________________");
        System.out.println("Aplikasi Pengolah Nilai Siswa");
        System.out.println("_______________________________________________________________");
        System.out.println("File telah di generate pada directori: src");
        System.out.println();
        System.out.println("0. exit");
        System.out.println("1. Kembali");
    }
    public static void read(String txtFile, String delimiter){
        Recorder rcd=new Recorder();
        try {
            File file = new File(txtFile);
            FileReader fr=new FileReader(file);
            BufferedReader br=new BufferedReader(fr);
            String line ="";
            String[] arr;

            while ((line=br.readLine()) !=null){
                List<String>nilaiKelas=new ArrayList<>();
                arr=line.split(delimiter);
                for (String str:arr
                ) {

                    nilaiKelas.add(str);
                    if (nilaiKelas.size()>1) {
                        Recorder.akumulasiNilai.add(Integer.parseInt(str));
                    }
                }
                rcd.nilaiSekolah.add(nilaiKelas);
                System.out.println();
            }
            br.close();
            fr.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
    public static void writerFrekuensi(){
        ReadAndWrite.read ("src\\data_sekolah.csv", ";");
        String fileName = "src/Frekuensi_Nilai.txt";

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write("Berikut Hasil Pengolahan Nilai"+'\n');
            fileWriter.write("   Nilai    |    Frekuensi"+'\n');

            Statistik.frekuensi();
            for (int j = 0; j <Recorder.frequensi.size(); j++) {
                for (int k = 0; k <Recorder.frequensi.get(j).size(); k++) {
                     fileWriter.write("    "+Recorder.frequensi.get(j).get(k)+"          ");

                }
            fileWriter.write("\n");

            }

            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }
    }
    public static void writerModusRatarataMedian(){
        ReadAndWrite.read ("src\\data_sekolah.csv", ";");
        String fileName = "src/ModusMeanMedian.txt";

        try {
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write("Berikut Hasil Pengolahan Nilai"+'\n');
            fileWriter.write("Berikut Hasil Sebaran Nilai"+'\n'+'\n');
            Statistik.avgtotal();
            Statistik.median();
            Statistik.modus();
            fileWriter.write("Mean     "+Recorder.avgKomulatif+'\n');
            fileWriter.write("Median   "+Recorder.median+'\n');
            fileWriter.write("Modus    "+Recorder.Modus+'\n');
            fileWriter.close();
        } catch (IOException e) {
            System.out.println("Terjadi kesalahan karena: " + e.getMessage());
        }
    }

}
